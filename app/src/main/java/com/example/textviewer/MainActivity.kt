package com.example.textviewer

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


open class MainActivity : AppCompatActivity() {
    /** variable for switch-button*/
    private var switcher = false
    /** variable for save position of the scrollbar */
    private var scrollPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // set text scrollable
        textForAppTextView.movementMethod = ScrollingMovementMethod()
        //get the text from file and view it on the screen
        setButtonEvent()
    }

    /** event for click*/
    private fun setButtonEvent() {
        myButton.setOnClickListener {
            switcher = !switcher
            setContent()

        }
    }

    /** switch the button text, button color, and text from source file  */
    private fun setContent() {
        textForAppTextView.scrollTo(0, scrollPosition)
        if (switcher) {
            setTextInApp(getTextFromAssets())
            setBtnStyle(R.drawable.main_btn_red, R.string.btn_text_hide)
        } else {
            setTextInApp("")
            setBtnStyle(R.drawable.main_btn_green, R.string.btn_text_show)
        }
    }

    /** set the text in text view*/
    private fun setBtnStyle(btnStyle: Int, btnText: Int) {
        myButton.setBackgroundResource(btnStyle)
        myButton.setText(btnText)
    }

    /** set the text in text view*/
    private fun setTextInApp(textFromFile: String) {
        textForAppTextView.text = textFromFile
    }

    /** open the source file and read the text, converts it in string*/
    private fun getTextFromAssets(): String {
        val filename: String = getString(R.string.filename)
        return applicationContext.assets.open(filename).reader().readText()
    }

    /** saving the state of the screen*/
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        scrollPosition = textForAppTextView.scrollY
        outState.putBoolean("switcher", switcher)
        outState.putInt("scrollPosition", scrollPosition)
    }

    /** restoring the state of the screen*/
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        switcher = savedInstanceState.getBoolean("switcher")
        scrollPosition = savedInstanceState.getInt("scrollPosition")
        setContent()
    }

}